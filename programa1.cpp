#include <bits/stdc++.h>
#include <time.h>

int main(){
	clock_t start, end;
	start = clock();
	time_t t;
	srand((unsigned)time(&t));
	int numero=536870912;
	int* array= new int [numero];
	int random;

	for (int i = 0; i < numero; ++i)
	{
		random = rand()%numero;
		array[i]=random;
	}

	end=clock();
	printf("tiempo ejecucion: %f\n", (double)(end-start)/CLOCKS_PER_SEC);


}